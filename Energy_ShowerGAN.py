import numpy as np
from tensorflow import keras
from keras.layers import *
from keras.models import Sequential, Model
from keras.optimizers import *
from keras.layers.advanced_activations import LeakyReLU
from functools import partial
from keras.utils import plot_model
import keras.backend.tensorflow_backend as KTF
import glob
import utils
import tensorflow as tf
import kerasAppendix as ka

log_dir = ka.CookbookInit()
#KTF.set_session(utils.get_session())  # Allows 2 jobs per GPU, Please do not change this during the tutorial

EPOCHS = 1
ntest = 5000
nval = 1000
GRADIENT_PENALTY_WEIGHT = 10
BATCH_SIZE = 64
NCR = 5
latent_size = 80  # size of noise for the generator input
energy_in = 1  # size of energy labels for the generator input
Rec_scaling = 1./1000  # 
# load trainings data
Folder = '/net/scratch/JGlombitza/Public/HAPWorkshop2018/airshower/Data_preprocessed_Xmax=const&Zenith=const'
filenames=glob.glob(Folder + "/PlanarWaveShower*")

shower_maps, Energy, showeraxis = utils.ReadInData(filenames)
shower_maps = shower_maps[:,:,:,1,np.newaxis]
utils.plot_multiple_signalmaps(shower_maps[:,:,:,0], log_dir=log_dir, title='Footprints', epoch='Real', labels=Energy)
shower_maps_test, shower_maps_val, shower_maps = np.split(shower_maps, [ntest, ntest+nval,]) # split into train, test, validation data
Energy_test, Energy_val, Energy = np.split(Energy, [ntest, ntest+nval,])
N = shower_maps.shape[0]

# --------------------------------------------------
# Set up generator, discriminator and GAN (stacked generator + discriminator)
# --------------------------------------------------

def build_generator(latent_size, energy_in):
    input1 = Input(shape=(latent_size,), name='noise')
    input2 = Input(shape=(energy_in,), name='label')
    inputs = Concatenate(axis=-1)([input1, input2])
    x = Dense(latent_size, activation='relu')(inputs)
    x = Reshape((1,1,latent_size))(x)
    x = UpSampling2D(size=(3,3))(x)
    x = Conv2D(64, (2, 2), padding='same', kernel_initializer='he_normal', activation='relu')(x)
    x = BatchNormalization()(x)
    x = UpSampling2D(size=(3,3))(x)
    x = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu')(x)
    x = BatchNormalization()(x)
    x = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu')(x)
    x = BatchNormalization()(x)
    x = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu')(x)
    x = BatchNormalization()(x)
    outputs = Conv2D(1, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu')(x)
    return Model(inputs=[input1, input2], outputs=outputs, name='generator')


def build_conditioner():
    rec = Sequential(name='conditioner')
    rec.add(Conv2D(64, (3, 3), padding='same', kernel_initializer='he_normal', input_shape=(9,9,1)))
    rec.add(LeakyReLU())
    rec.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal'))
    rec.add(LeakyReLU())
    rec.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal'))
    rec.add(LeakyReLU())
    rec.add(Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal'))
    rec.add(LeakyReLU())
    rec.add(GlobalMaxPooling2D())
    rec.add(Dropout(0.3))
    rec.add(Dense(100))
    rec.add(LeakyReLU())
    rec.add(Dropout(0.3))
    rec.add(Dense(1))
    return rec


def build_critic():
    critic = Sequential(name='critic')
    critic.add(Conv2D(64, (3, 3), padding='same', kernel_initializer='he_normal', input_shape=(9,9,1)))
    critic.add(LeakyReLU())
    critic.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal'))
    critic.add(LeakyReLU())
    critic.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal'))
    critic.add(LeakyReLU())
    critic.add(Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal'))
    critic.add(LeakyReLU())
    critic.add(GlobalMaxPooling2D())
    critic.add(Dense(100))
    critic.add(LeakyReLU())
    critic.add(Dense(1))
    return critic


# build generator, critic and conditioner
generator = build_generator(latent_size, energy_in)
plot_model(generator, to_file=log_dir + '/generator.png', show_shapes=True)
critic = build_critic()
plot_model(critic, to_file=log_dir + '/critic.png', show_shapes=True)
conditioner = build_conditioner()
#conditioner.compile(Adam(0.001), loss='mse')
#conditioner.fit(shower_maps, Energy, epochs=20)
#conditioner.save('conditioner.h5')

# make trainings model for generator
utils.make_trainable(critic, False)
utils.make_trainable(conditioner, False)
utils.make_trainable(generator, True)

generator_training = utils.build_generator_graph(generator, critic, conditioner, latent_size, energy_in)
generator_training.compile(optimizer=Adam(0.0003, beta_1=0.5, beta_2=0.9, decay=0.0001), loss=[utils.wasserstein_loss, 'mse'], loss_weights=[1, Rec_scaling])
plot_model(generator_training, to_file=log_dir + '/generator_training.png', show_shapes=True)

# make trainings model for critic
utils.make_trainable(critic, True)
utils.make_trainable(conditioner, True)
utils.make_trainable(generator, False)

# create gradient penalty loss
critic_training, averaged_batch = utils.build_critic_graph(generator, critic, conditioner, latent_size, energy_in, batch_size=BATCH_SIZE)
gradient_penalty = partial(utils.gradient_penalty_loss, averaged_batch=averaged_batch, penalty_weight=GRADIENT_PENALTY_WEIGHT)
gradient_penalty.__name__ = 'gradient_penalty'
critic_training.compile(optimizer=Adam(0.0003, beta_1=0.5, beta_2=0.9, decay=0.0001), loss=[utils.wasserstein_loss, utils.wasserstein_loss, gradient_penalty, 'mse'], loss_weights=[1,1,1, Rec_scaling])
plot_model(critic_training, to_file=log_dir + '/critic_training.png', show_shapes=True)


# For Wassersteinloss
positive_y = np.ones(BATCH_SIZE)
negative_y = -positive_y
dummy = np.zeros(BATCH_SIZE) # keras throws an error when calculating a loss withuot having a label -> needed for using the gradient penalty loss

generator_loss = []
critic_loss = []


# trainings loop
iterations_per_epoch = N//((NCR+1)*BATCH_SIZE)
for epoch in range(EPOCHS):
    print "epoch: ", epoch
    # plot generated footprints
    energy_label = np.random.uniform(1,100,(nval,energy_in))
    generated_map = generator.predict_on_batch([np.random.randn(nval, latent_size), energy_label/100.])
    utils.plot_multiple_signalmaps(generated_map[:,:,:,0], log_dir=log_dir, title='Generated Footprints Epoch: ', epoch=str(epoch), labels=energy_label[:,0])
    # plot reconstructed energy labels against input label
    predicted_Energy = conditioner.predict(generated_map)
    utils.plot_energy_histos(energy_label[:,0], predicted_Energy, log_dir, name=str(epoch))

    energy_label =  np.repeat(np.array([10,50,75,100]),energy_in).reshape(4,energy_in)
    generated_map = generator.predict_on_batch([np.random.randn(4, latent_size), energy_label/100.])
    utils.plot_multiple_signalmaps(generated_map[:,:,:,0], log_dir=log_dir, title='Generated footprints epoch', epoch=str(epoch), labels=energy_label[:,0], shuffle=False)

    for iteration in range(iterations_per_epoch):

        for j in range(NCR):
            #  critic training
            energy_batch = Energy[BATCH_SIZE*(j+iteration):BATCH_SIZE*(j+iteration+1)]
            shower_batch = shower_maps[BATCH_SIZE*(j+iteration):BATCH_SIZE*(j+iteration+1)]
            energy_label = np.random.uniform(1,100,(BATCH_SIZE, energy_in))
            noise_batch = np.random.randn(BATCH_SIZE, latent_size)  # generate noise batch for generator times Energy
            critic_loss.append(critic_training.train_on_batch([noise_batch, shower_batch, energy_label/100.], [negative_y, positive_y, dummy, energy_batch]))
            print "critic loss:", critic_loss[-1]
        #  generator training
        energy_label = np.random.uniform(1,100,(BATCH_SIZE, energy_in))
        noise_batch = np.random.randn(BATCH_SIZE, latent_size)  # generate noise batch for generator
        generator_loss.append(generator_training.train_on_batch([noise_batch, energy_label/100.], [positive_y, energy_label[:,0]]))
        print "generator loss:", generator_loss[-1]

# plot losses
utils.plot_loss(critic_loss, name=str(epoch) + "_full_critic", iterations_per_epoch=iterations_per_epoch, NCR=NCR, log_dir=log_dir, Rec_scaling=Rec_scaling)
utils.plot_loss_wasserstein(critic_loss, name=str(epoch) + "_full_critic", iterations_per_epoch=iterations_per_epoch, NCR=NCR, log_dir=log_dir, Rec_scaling=Rec_scaling)
utils.plot_loss(generator_loss, name="generator", iterations_per_epoch=iterations_per_epoch, log_dir=log_dir)
utils.plot_loss_reco(generator_loss, name="generator", iterations_per_epoch=iterations_per_epoch, log_dir=log_dir)

# plot results
energy_label = np.random.uniform(1,100,(ntest, energy_in))
generated_map = generator.predict_on_batch([np.random.randn(ntest, latent_size), energy_label/100.])
predicted_Energy = conditioner.predict(generated_map)
utils.plot_energy_histos(energy_label[:,0], predicted_Energy, log_dir, name="AfterTraining: Generated footprints")  # plot reconstructed energy for generated footprints
predicted_Energy = conditioner.predict(shower_maps_test)
utils.plot_energy_histos_real(Energy_test, predicted_Energy, log_dir, name="AfterTraining: Real footprints")  # plot reconstructed energy for real footprints

utils.plot_multiple_signalmaps(generated_map[:,:,:,0], log_dir=log_dir, title='Generated Footprints', epoch='Final', labels=energy_label[:,0])

energy_label =  np.repeat(np.array([10,50,75,100]),energy_in).reshape(4,energy_in)
generated_map = generator.predict_on_batch([np.random.randn(4, latent_size), energy_label/100.])
utils.plot_multiple_signalmaps(generated_map[:,:,:,0], log_dir=log_dir, title='Test Footprints', epoch='Final', labels=energy_label[:,0], shuffle=False)
